#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>  
#include <string.h>
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <sys/time.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <signal.h>

#define LOCAL_HOST_ADDR "127.0.0.1"
#define ever (;;)
#define MAX_CLIENTS 50
#define MAX_PENDING 10
#define LISTENING_PORT "9004"
#define UPLOAD_STR "upload"
#define DOWNLOAD_STR "download"

int fdHeartbeatSocket;
struct sockaddr_in heartbeatAddress;
int clients[MAX_CLIENTS];
int masterSocket;
fd_set readFds;
int selected;

void createHeartBeatMsg(char* heartBeatPort);
void initHeartBeatSocket(char* heartBeatPort);
void signalHeartbeat();

bool validateArguments(int argc, char* argv[]);
void initServerSocket();
void listenToClients();
void handleNewConnection();
void handleCommands();
void handleDownloadCommand(char* cmd, int clientFd);
void handleUploadCommand(char* cmd, int clientFd);

bool isFileExist(char* filename);
void writeToFile(char* filename, char* data, int bytes);
int readFromFile(char* filename, char* data);

#endif