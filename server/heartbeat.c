#include "server.h"
bool isHeartbeating = false;
char heartbeatMsg[1025];

void createHeartBeatMsg(char* heartBeatPort)
{
    strcat(heartbeatMsg, LOCAL_HOST_ADDR);
    strcat(heartbeatMsg, " ");
    strcat(heartbeatMsg, LISTENING_PORT);
}

void initHeartBeatSocket(char* heartBeatPort)
{
    if ((fdHeartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        write(1, "making socket failed", 22);
        return;
    }

    heartbeatAddress.sin_family = AF_INET;
    heartbeatAddress.sin_port = htons(atoi(heartBeatPort));
    heartbeatAddress.sin_addr.s_addr = inet_addr(LOCAL_HOST_ADDR);
}

void signalHeartbeat()
{
    if (sendto(fdHeartbeatSocket, heartbeatMsg, strlen(heartbeatMsg), 0, (struct sockaddr*)&heartbeatAddress, sizeof(heartbeatAddress)) > -1) {
        if(!isHeartbeating) {
            isHeartbeating = true;
            write(1, "<3 Hearbeating...\n", 20);
        } 
    }
    
    signal(SIGALRM, signalHeartbeat);
    alarm(1);
}