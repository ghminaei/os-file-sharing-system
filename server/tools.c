#include "server.h"

bool isFileExist(char* filename)
{
    if (access(filename, F_OK) != -1 ) {
        return true;
    }
    return false;
}

void writeToFile(char* filename, char* data, int bytes)
{
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (fd == 0) {
        return -1;
    }

    write(fd, data, bytes);
    close(fd);
}

int readFromFile(char* filename, char* data)
{
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (fd == 0) {
        return -1;
    }

    int bytesRead = read(filename, data, sizeof(data));
    close(fd);
    return bytesRead;
}
