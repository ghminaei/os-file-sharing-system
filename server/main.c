#include "server.h"

int main(int argc, char *argv[])
{
    if (!validateArguments(argc, argv)) {
        write(1, "Input should be like: \"server heartbeat_port_X\"\n", 50);
        return 0;
    }

    int heartbeatProcess = fork();
    if (heartbeatProcess > 0) {
        createHeartBeatMsg(argv[1]);
        initHeartBeatSocket(argv[1]);
        signalHeartbeat();
        for ever {}
    } else {
        initServerSocket();
        listenToClients();
    }
    
    for ever {}
    return 1;
}