#include "server.h"

bool validateArguments(int argc, char* argv[])
{
    if (argc != 2) return false;
    //todo: add being valid socket number
    return true;
}

void initServerSocket()
{
    int opt = 1;
    struct sockaddr_in serverAddress;  

    for (int i = 0; i < MAX_CLIENTS; i++) {   
        clients[i] = 0;   
    }

    if ((masterSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {   
        write(1, "socket failed", 15);   
        exit(EXIT_FAILURE);   
    }

    if (setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
        write(1, "set socket option", 19);   
        exit(EXIT_FAILURE);   
    }

    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(LOCAL_HOST_ADDR);
    serverAddress.sin_port = htons(atoi(LISTENING_PORT)); 

    if (bind(masterSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {   
        write(1, "bind failed", 13);   
        exit(EXIT_FAILURE);   
    }
          
    if (listen(masterSocket, MAX_PENDING) < 0) {
        write(1, "listen failed", 15);
        exit(EXIT_FAILURE);
    }
}

void listenToClients()
{
    int maximumFd, socketDescriptor;
    for ever {   
        FD_ZERO(&readFds);
        FD_SET(masterSocket, &readFds);
        maximumFd = masterSocket;
             
        for (int i = 0 ; i < MAX_CLIENTS ; i++) {
            socketDescriptor = clients[i];
            if (socketDescriptor > 0)
                FD_SET(socketDescriptor, &readFds);
            if (socketDescriptor > maximumFd)
                maximumFd = socketDescriptor;   
        }   
      
        selected = select(maximumFd + 1, &readFds, NULL, NULL, NULL);
       
        if ((selected < 0) && (errno != EINTR)) {
            write(1, "select failed", 15);
        }
        handleNewConnection();
        handleCommands();
    }
}

void handleNewConnection()
{
    int newSocket;
    struct sockaddr_in serverAddress;
    int addressLength;
    memset(&serverAddress, 0, sizeof(serverAddress));
    if (FD_ISSET(masterSocket, &readFds)) {   
        if ((newSocket = accept(masterSocket, (struct sockaddr *)&serverAddress, (socklen_t *) &addressLength)) < 0) {
            write(1, "accept failed", 15);   
            exit(EXIT_FAILURE);   
        }   

        char* msg = "Welcome to server...\n";

        // printf("New connection , socket fd is %d , ip is : %s , port : %d \n",
        //         newSocket,
        //         inet_ntoa(serverAddress.sin_addr),
        //         ntohs(serverAddress.sin_port)
        if (send(newSocket, msg, strlen(msg), 0) != strlen(msg)) {
            write(1, "send failed", 13);
        }   
                 
        write(1, "welcome message sent to client.\n", 34);
        for (int i = 0; i < MAX_CLIENTS; i++) {
            if (clients[i] == 0 ) {
                clients[i] = newSocket;     
                break;   
            }   
        }   
    }  
}

void handleCommands()
{
    int socketDescriptor, bytesRead;
    int addressLength;
    struct sockaddr_in serverAddress;
    char buffer[1025];
    for (int i = 0; i < MAX_CLIENTS; i++) {
        socketDescriptor = clients[i];
        if (FD_ISSET(socketDescriptor, &readFds)){
            memset(buffer, '\0', sizeof(buffer));
            if ((bytesRead = read(socketDescriptor, buffer, sizeof(buffer)-1)) == 0) { 
                getpeername(socketDescriptor, (struct sockaddr *) &serverAddress, (socklen_t *) &addressLength);
                // printf("Host disconnected, ip %s, port %d\n",
                //        inet_ntoa(serverAddress.sin_addr),
                //        ntohs(serverAddress.sin_port)
                // );//todo: change to write
                close(socketDescriptor);
                clients[i] = 0;
            } else {
                buffer[bytesRead] = '\0';
                if ((strncmp(buffer, UPLOAD_STR, 6)) == 0) {
                    write(1, "uploading\n", 11);
                    handleUploadCommand(buffer, socketDescriptor);
                    write(1, "done\n", 6);
                } else if ((strncmp(buffer, DOWNLOAD_STR, 8)) == 0) {
                    write(1, "downloading\n", 13);
                    handleDownloadCommand(buffer, socketDescriptor);
                    write(1, "done\n", 6);
                } else {

                }
            }   
        }   
    }   
}

void handleDownloadCommand(char* cmd, int clientFd)
{
    char* filename;
    char* data;
    char fileDosentExistMsg[1025] = "@failed";
    //parseCommand(cmd, filename, data);//get filename
    filename = strtok(cmd, " ");
    bool fileExist = isFileExist(filename);
    if (!fileExist) {
        write(clientFd, fileDosentExistMsg, strlen(fileDosentExistMsg));
        return;
    }

    writeToFile(filename, data, sizeof(data));
}

void handleUploadCommand(char* cmd, int clientFd)
{
    char* filename;
    char* data;
    filename = strtok(cmd, " ");
    filename = strtok(NULL, " ");
    int valRead = readFromFile(filename, data);
    write(clientFd, data, valRead);
}