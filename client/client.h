#ifndef CLIENT_H
#define CLIENT_H

#include <stdio.h>  
#include <string.h>
#include <stdlib.h>  
#include <errno.h>  
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <sys/time.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/time.h>


#define LOCAL_HOST_ADDR "127.0.0.1"
#define ever (;;)
#define UPLOAD_STR "upload"
#define DOWNLOAD_STR "download"
#define MAX_PEERS 10
#define MAX_PENDING 3
#define STRING_LENGTH 1024

int fdBrodcastSocket;
struct sockaddr_in broadcastAddress;
int fdHeartbeatSocket;
struct sockaddr_in serverAddress;
int peers[MAX_PEERS];
int masterSocket;
fd_set readFds;
int selected;
char tcpPort[6];

bool validateArguments(int argc, char* argv[]);
void initBroadcastingSocket(char* broadcastPort);
void initHeartbeatSocket(char* heartbeatPort);
void listenToPeers();
void handleBroadcastedRequest();
void handleTeminalCommands(char* cmd);
void handleDownloadCommand(char* cmd, int clientFd);
void handleUploadCommand(char* cmd, int clientFd);
bool isServerAlive();
void broadcastRequest();
void getServerInformation(char* string);

bool isFileExist(char* filename);
void writeToFile(char* filename, char* data, int bytes);
int readFromFile(char* filename, char* data);

#endif