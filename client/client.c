#include "client.h"
int broadcasting = false;
char broadcastingMsg[1025];

bool validateArguments(int argc, char* argv[])
{
    if (argc != 4) return false;
    //todo: add being valid socket number
    return true;
}


void initBroadcastingSocket(char* broadcastPort)
{
    int opt = 1;

    for (int i = 0; i < MAX_PEERS; i++) {   
        peers[i] = 0;   
    }

    if ((masterSocket = socket(AF_INET, SOCK_DGRAM, 0)) == 0) {   
        write(1, "socket failed", 15);   
        exit(EXIT_FAILURE);   
    }

    if (setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
        write(1, "set socket option", 19);   
        exit(EXIT_FAILURE);   
    }

    memset(&broadcastAddress, 0, sizeof(broadcastAddress));
    broadcastAddress.sin_family = AF_INET;
    broadcastAddress.sin_port = htons(atoi(broadcastPort));
    broadcastAddress.sin_addr.s_addr = inet_addr(LOCAL_HOST_ADDR); 

    if (bind(masterSocket, (struct sockaddr *)&broadcastAddress, sizeof(broadcastAddress)) < 0) {   
        write(1, "bind failed", 12);   
        exit(EXIT_FAILURE);   
    }
          
}

void listenToPeers()
{
    int maximumFd, socketDescriptor;
    int newSocket, n;
    char buffer[1025];
    for ever {   
        FD_ZERO(&readFds);
        FD_SET(masterSocket, &readFds);
        FD_SET(STDIN_FILENO, &readFds);
        memset(&buffer, '\0', sizeof(buffer));
        maximumFd = masterSocket;
             
        for (int i = 0 ; i < MAX_PEERS ; i++) {
            socketDescriptor = peers[i];
            if (socketDescriptor > 0)
                FD_SET(socketDescriptor, &readFds);
            if (socketDescriptor > maximumFd)
                maximumFd = socketDescriptor;   
        }   
      
        selected = select(maximumFd + 1, &readFds, NULL, NULL, NULL);
       
        if ((selected < 0) && (errno == EINTR)) {
            continue;
        }

        if (FD_ISSET(masterSocket, &readFds)) {   
            struct sockaddr_in peerAddress;
            socklen_t addrLen = sizeof peerAddress;
            memset(&peerAddress, 0, sizeof(peerAddress));
            n = recvfrom(masterSocket, buffer, sizeof(buffer), 0, (struct sockaddr*)&peerAddress, addrLen);
            buffer[sizeof(buffer)] = '\0';
        }
        
        if (FD_ISSET(STDIN_FILENO, &readFds)) {
            read(1, buffer, sizeof(buffer)-1);
            buffer[sizeof(buffer)] = '\0';
            handleTeminalCommands(buffer);
            
            read(1, buffer, sizeof(buffer)-1);
        }
    }
}

void handleTeminalCommands(char* cmd)
{
    char serverResponse[STRING_LENGTH];
    char buffer[STRING_LENGTH];
    char request[4096];
    memset(&serverResponse, '\0', sizeof(serverResponse));
    if (strncmp(cmd, UPLOAD_STR, 6) == 0) {
        char* filename = strtok(cmd, " ");
        filename = strtok(NULL, "\n");
        write(1, "filename: ", 11);
        write(1, filename, sizeof(filename));
        write(1, "\n", 2);
        if (!isFileExist(filename)) {
            write(1, "file dosen't exist\n", 20);
            return;
        }
        if (!isServerAlive()) {
            write(1, "server is not alive\n", 21);
            return;
        }
        int bytes = readFromFile(filename, buffer);
        int clientSocket;
        clientSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (connect(clientSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
            write(1, "connect failed\n", 16);
            exit(EXIT_FAILURE);
        }
        if(recv(clientSocket, &serverResponse, sizeof(serverResponse), 0)) {
            write(1, serverResponse, sizeof(serverResponse));
            strcat(request, cmd);
            // strcat(request, filename);
            strcat(request, "@");
            strcat(request, buffer);
            write(clientSocket, &request, sizeof(request));
            memset(&serverResponse, '\0', sizeof(serverResponse));
        }
    } else if (strncmp(cmd, DOWNLOAD_STR, 8) == 0) {
        int valRead;
        char* filename = strtok(cmd, " ");
        if (isServerAlive()) {
            write(1, "server is alive\n", 17);
            int clientSocket;
            clientSocket = socket(AF_INET, SOCK_STREAM, 0);
            if (connect(clientSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
                write(1, "connect failed\n", 16);
                exit(EXIT_FAILURE);
            }
            if(recv(clientSocket, &serverResponse, sizeof(serverResponse), 0)) {
                write(1, serverResponse, sizeof(serverResponse));
                write(clientSocket, &cmd, sizeof(cmd));
                memset(&serverResponse, '\0', sizeof(serverResponse));
                valRead = read(clientSocket, &serverResponse, sizeof(serverResponse));
            }
            if (strncmp(serverResponse, "@failed", 8)) {
                write(1, "file is not on server\n", 23);
                handleBroadcastedRequest(cmd);
            } else {
                writeToFile(filename, serverResponse, valRead);
            }
            
        } else {
            handleBroadcastedRequest(cmd);
        }
    } else {
         write(1, "I don't get you!\n", 18);
         return;   
    }
}

void initHeartbeatSocket(char* heartbeatPort)
{
    struct sockaddr_in addressOfSocket;
    char recvString[STRING_LENGTH+1];
    int recvStringLen;    
    
    if ((fdHeartbeatSocket = socket(AF_INET , SOCK_DGRAM, 0)) < 0) {
        write(2, "fail to open socket\n", 21);
        return ;
    }
    int broadcast = 1;
    if (setsockopt(fdHeartbeatSocket, SOL_SOCKET, SO_REUSEADDR, &broadcast, sizeof(broadcast)) == -1) {
        write(2, "fail to set broadcast\n", 23);
        return ;
    }

    addressOfSocket.sin_family = AF_INET;
    addressOfSocket.sin_port = htons(atoi(heartbeatPort));
    addressOfSocket.sin_addr.s_addr = htonl(INADDR_ANY);

    
    if( bind(fdHeartbeatSocket, (struct sockaddr *) &addressOfSocket , sizeof(addressOfSocket)) < 0) {
        write(2, "fail to bind", 13);
        return ;
    }

    float timeout = 4;
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = timeout * 1000;
    if (setsockopt(fdHeartbeatSocket, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
        write(1, "unable to set timeout", 22);
    }
}

bool isServerAlive()
{
    char recvString[STRING_LENGTH+1];
    int recvStringLen;  
    if ((recvStringLen = recvfrom(fdHeartbeatSocket, recvString, STRING_LENGTH, 0, NULL , 0)) > 0) {
        recvString[recvStringLen] = '\0';
        write(1, "server information :", 21);
        write(1, recvString, strlen(recvString));
        write(1, "\n", 1);
        getServerInformation(recvString);
        return true;
    }
    return false;
}

void getServerInformation(char* string)
{
    serverAddress.sin_family = AF_INET;
    char* address;
    int port;
    address = strtok(string, " ");
    port = atoi(strtok(NULL, " "));
    serverAddress.sin_addr.s_addr = inet_addr(address);
    serverAddress.sin_port = htons(port);
}

void handleBroadcastedRequest(char* cmd)
{
    strcat(broadcastingMsg, cmd);
    strcat(broadcastingMsg, " ");
    strcat(broadcastingMsg, tcpPort);
    write(1, broadcastingMsg, sizeof(broadcastingMsg));
    broadcastRequest();
}

void broadcastRequest()
{
    if (sendto(masterSocket, broadcastingMsg , strlen(broadcastingMsg), 0,
     (struct sockaddr*)&broadcastAddress, sizeof(broadcastAddress)) > -1) {
        if (!broadcasting) {
            write(1, "Broadcasting.", 14);
            broadcasting = true;
        }   
        else {
            write(1, ".", 1);
        }
    }
    else {
        write(1, "broadcasting error\n", 20);
    }
     
    signal(SIGALRM, broadcastRequest);
    alarm(3);
}