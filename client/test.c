#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int isFileExist(char* filename)
{
    if (access(filename, F_OK) != -1 ) {
        return 1;
    }
    return 2;
}

int readFromFile(char* filename, char data[])
{
    // int fd = open(filename, O_RDONLY);
    // if (fd == 0) {
    //     return -1;
    // }
    int fd1 = open(filename, O_RDONLY, 0); 
    int bytesRead = read(filename, &data, sizeof(data));
    printf("c = %s\n", data);
    close(fd1);
    return bytesRead;
}


int main() {

    // char data[1024];
    // char filename[] = "test.txt";
    // int d;
    // //int d = readFromFile(filename, data);
    // int fd = open("test.txt", O_RDONLY, 0);
    // if (fd == 0) {
    //     d = -1;
    // }
    // d = read(filename, data, sizeof(data));
    // close(fd);
    // printf("%d\n", d);
    // printf("data %s\n", data);

    char c[200];
    char* filename = "foobar.txt";
    // int fd1 = open(filename, O_RDONLY, 0); 
    // read(fd1, &c, sizeof(c));
    readFromFile(filename, c);
    printf("c = %s\n", c);
    return 0;
}