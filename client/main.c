#include "client.h"

int main(int argc, char *argv[])
{
    if (!validateArguments(argc, argv)) {
        write(1, "Input should be like: \"Client heartbeat_port_X broacast_port_Y client_port_M\"\n", 80);
        return 0;
    }

    strcat(tcpPort, argv[3]);
    initHeartbeatSocket(argv[1]);
    initBroadcastingSocket(argv[2]);
    listenToPeers();
}